//
//  ReferralViewController.swift
//  BW22
//
//  Created by PSE on 20/04/2022.
//

import UIKit

class ReferralViewController: UIViewController {
    
    
    @IBOutlet weak var txtReferralCode: UILabel!
    
    @IBOutlet weak var imgShare: UIImageView!
    
    @IBOutlet weak var txtFollowers: UILabel!
    
    @IBOutlet weak var txtActive: UILabel!
    
    var referralCode : String = ""
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var shareStackView: UIStackView!
    @IBOutlet weak var imgQr: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFromLocalsFirst()
        let phone = UDM.shared.getStringValue(Constants.phoneNumber)
        print(phone)
        requestReferralCode(phoneNumber: phone)
     
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(shareCode))
            self.shareStackView.addGestureRecognizer(gestureRecognizer)
        
        // click on notification icon
        let gestureRecognizerI = UITapGestureRecognizer(target: self, action: #selector(showNotification))
            self.imgBack.addGestureRecognizer(gestureRecognizerI)
    }
    
    @objc func shareCode() -> Void {
        
            let sharingText = "Hi, please use my referral code to login: \(Constants.SharingReferralCodeURL+referralCode)"
            let activityViewController = UIActivityViewController(activityItems: [sharingText] , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
        // This line remove the arrow of the popover to show in iPad
            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: (self.view.bounds.midX), y: (self.view.bounds.midY - 80), width: 1.0, height: 1.0)
            
            self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    @objc func showNotification(){
        showSnackBar("No notifications found")
    }
    
    
         func showSnackBar(_ message:String){
             MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
         }

    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    
    func requestReferralCode(phoneNumber:String){
        let url = URL(string: Constants.RequestReferralCodeURL+phoneNumber)
        
            URLSession.shared.request(url: url, model: WrappedResponse<UserReferralResponse>.self){
                result in
                DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.handleRequestURLSuccessResponse(response)
                case .failure(let error):
                    self.handleRequestURLFailureResponse(error)
                }
            }
        }
     
    
    }
    
    func handleRequestURLSuccessResponse(_ response:UserReferralResponse){
        referralCode  = response.referral_code
        txtReferralCode.text = referralCode
        print(response.number_of_active_user)
        
        txtFollowers.text = "\(response.number_of_followers)"
        txtActive.text = "\(response.number_of_active_user)"
        
        // generate QR code
        let image = generateQRCode(from: "https://www.bluewhale.host/ref.php?refcode=\(referralCode)")
        imgQr.image = image
        
        saveInLocals(response)
        
    }
    
    func saveInLocals(_ response:UserReferralResponse){
        UDM.shared.saveString("\(response.number_of_followers)", Constants.followers)
        UDM.shared.saveString("\(response.number_of_active_user)", Constants.actives)
        UDM.shared.saveString(response.referral_code, Constants.referralCodeLocal)
    }
    
    func loadFromLocalsFirst(){
         let referralCodeLocal = UDM.shared.getStringValue(Constants.referralCodeLocal)
         let followers = UDM.shared.getStringValue(Constants.followers)
         let actives = UDM.shared.getStringValue(Constants.actives)
        
        referralCode  = referralCodeLocal
        txtReferralCode.text = referralCodeLocal
        
        
        txtFollowers.text = followers
        txtActive.text = actives
        
        // generate QR code
        let image = generateQRCode(from: "https://www.bluewhale.host/ref.php?refcode=\(referralCodeLocal)")
        imgQr.image = image
    }
    
    
    
    
    func handleRequestURLFailureResponse(_ error:Error){
        switch error {
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
    }

}
