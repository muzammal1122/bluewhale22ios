//
//  HomeViewController.swift
//  BW22
//
//  Created by PSE on 20/04/2022.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var maxBookingAmountLAble: UILabel!
    
    @IBOutlet weak var preBookingLable: UILabel!
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var scrollViewView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var txtContactUs: UILabel!
    
    @IBOutlet weak var txtMarketCap: UILabel!
    
    @IBOutlet weak var txtPreBooking: UILabel!

    @IBOutlet weak var imgQuestion: UIImageView!
    
    @IBOutlet weak var txtTotalBalance: UILabel!
    
    @IBOutlet weak var txtName: UILabel!
    
    @IBOutlet weak var etAddMore: UITextField!
    
    @IBOutlet weak var txtUsdtValue: UILabel!
    
    @IBOutlet weak var txtUsdt: UILabel!
    
    var localSavedName = "--"
    
    
    @IBOutlet weak var txtPercentage: UILabel!
    
    @IBOutlet weak var CircularProgress: CircularProgressView!
    
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var bottomView: UIView!{
        didSet{
            bottomView.layer.cornerRadius = 18
            bottomView.layer.shadowOpacity = 0.7
            bottomView.layer.shadowOffset = CGSize(width: 3, height: 3)
            bottomView.layer.shadowRadius = 15.0
            bottomView.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    
    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var addButtonOutlet: UIButton! {
        didSet{
            addButtonOutlet.isEnabled = false
            addButtonOutlet.alpha = 0.9
            addButtonOutlet.layer.cornerRadius = 12
            addButtonOutlet.clipsToBounds = true

        }
    }
    
    let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var change_percentage: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CircularProgress.trackClr = UIColor.gray
        CircularProgress.progressClr = UIColor(named: "DarkBlue") ?? UIColor.blue
        
        
      
        loadFromLocalsFirst()
         //click on contactus
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToContact))
            self.txtContactUs.addGestureRecognizer(gestureRecognizer)

        // click on img Question
        let gestureRecognizerQ = UITapGestureRecognizer(target: self, action: #selector(showPopUP))
            self.imgQuestion.addGestureRecognizer(gestureRecognizerQ)
        
        // click on notification icon
        let gestureRecognizerI = UITapGestureRecognizer(target: self, action: #selector(showNotification))
            self.imgBack.addGestureRecognizer(gestureRecognizerI)
        
        etAddMore.addTarget(self, action:#selector(self.edited), for:UIControl.Event.editingChanged)
        
        let userID = UDM.shared.getStringValue(Constants.phoneNumber)
        
        if(userID == "nil"){
            showSnackBar("phone_number_missing".localized)
        }
        else{
            self.getUserStats(userID)
        }
        
     
        
        hideKeyboardWhenTappedAround()

             
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if (self.navigationController?.topViewController == self) {
            //the view is currently displayed
            self.txtName.text = UDM.shared.getStringValue(Constants.firstName)+" "+UDM.shared.getStringValue(Constants.lastName)
    }
    
    
    func loadFromLocalsFirst(){
        let userTotalBalance = UDM.shared.getStringValue(Constants.userTotalBalance)
        let userUSDT = UDM.shared.getStringValue(Constants.userUSDT)
        let changeInPercentage = UDM.shared.getStringValue(Constants.changeInPercentage)
        let usdt = UDM.shared.getStringValue(Constants.usdt)
        let prebooking = UDM.shared.getStringValue(Constants.prebooking)
        let marketCap = UDM.shared.getStringValue(Constants.marketCap)
        let isUP = UDM.shared.getBoolValue(Constants.isUP)
        
        self.txtTotalBalance.text = userTotalBalance
        self.txtUsdt.text = userUSDT
        self.change_percentage.text = changeInPercentage
        self.txtUsdtValue.text = usdt
        self.txtPreBooking.text = prebooking
        self.txtMarketCap.text = marketCap
        
        if(isUP){
            self.arrow.image = UIImage(named: "Vectorup")
            self.change_percentage.textColor = UIColor(named: "greenArrowColor")
        }else{
            self.arrow.image = UIImage(named: "Vectordown")
            self.change_percentage.textColor = UIColor(named: "redArrowColor")
        }
        
    }
    
    
    @objc func showNotification(){
        showSnackBar("No notifications found")
    }

    
    @objc
    func edited() {
        let txt = etAddMore.text
        if(txt == ""){
            addButtonOutlet.isEnabled = false
            addButtonOutlet.alpha = 0.9
        }
        else {
            addButtonOutlet.isEnabled = true
            addButtonOutlet.alpha = 1
        }
    
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        if(UDM.shared.getBoolValue("isDialogShow") == false){
         PrizeViewController.showPopup(parentVC: self)
        }
       
//        txtName.text = UDM.shared.getStringValue(Constants.firstName)+" "+UDM.shared.getStringValue(Constants.lastName)

    }
    
    
    
    
    
  
    
    @IBAction func btnAddMore(_ sender: UIButton) {
       
        print("show popup")
        view.endEditing(true)
        let phoneNumber = UDM.shared.getStringValue(Constants.phoneNumber)
        let amount = etAddMore.text ?? ""
        let amountInt = (Int(amount)) ?? -1
        
        if (amountInt > 1000) {
            self.showSnackBar("max_booking_amount".localized)
        }
         else if (amountInt == -1) {
             self.showSnackBar("invalid_amount".localized)
        } else {
            if(amount == ""){
                self.showSnackBar("invalid_amount".localized)
            }else{
                let addMoreBody = AddMoreBody(booking_amount: amount, phone_number: phoneNumber)
                self.showLoading()
                self.addMore(addMoreBody)
            }
        }
    }
    
    
    @objc func goToContact() -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func showPopUP() -> Void {
        InfoViewController.showPopup(parentVC: self)
        
    }
    
    
    
    func getUserStats(_ userID:String){
        print("getUserStats called")
        URLSession.shared.request(url: URL(string: (Constants.UserStatsURL+userID)), model: WrappedResponse<UserStatsResponse>.self){
            result in
            
            DispatchQueue.main.async {
                //self.hideLoading()
                switch result {

                case .success(let response):
                    self.handleUserStatsSuccess(response)
                case .failure(let error):
                    self.handleUserStatsFailure(error)
                }
            }
        }
        
        
    }
    
    func addMore(_ addMoreBody: AddMoreBody){
        print("getUserStats called")
        URLSession.shared.requestUpload(url: Constants.AddMoreURL, type: "PUT", body: addMoreBody, model: WrappedResponse<AddMoreResponse>.self){
            result in
            DispatchQueue.main.async {
                self.hideLoading()
                switch result {

                case .success(let response):
                    self.handleAddMoreSuccess(response)
                case .failure(let error):
                    self.handleAddMoreFailure(error)
                }
            }
        }
        
        
    }
    
    
    func handleUserStatsFailure(_ error:Error){
        switch error {
        case CustomError.invalidBodyJson:
            MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
            
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
        
}
    func handleAddMoreFailure(_ error:Error){
        switch error {
        case CustomError.invalidBodyJson:
            MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
            
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
        
}
    
    func handleAddMoreSuccess (_ stats:AddMoreResponse){
        self.txtTotalBalance.text = "\(stats.bw22_total_balance.withCommas())"
        self.txtUsdt.text = "\(stats.user_usdt_totoal_balance.withCommas())"
        self.change_percentage.text = "\(stats.change_in_24_hours_percentage)%"
        self.txtUsdtValue.text = "\(stats.usdt_value)"
        self.txtPreBooking.text = "\(stats.market_total_pre_booking_value.withCommas())"
        self.txtMarketCap.text = "\(stats.market_cap_usdt_value.withCommas())"
        if(stats.is_up){
            self.arrow.image = UIImage(named: "Vectorup")
            self.change_percentage.textColor = UIColor(named: "greenArrowColor")
        }else{
            self.arrow.image = UIImage(named: "Vectordown")
            self.change_percentage.textColor = UIColor(named: "redArrowColor")
        }
        saveInLocals(stats)
        
        // do for progressbar
        let a = Float(stats.total_pre_booked_amount)
        let b = Float(1000)
        let c = a / b
        CircularProgress.setProgressWithAnimation(duration: 1.0, value: c)
        txtPercentage.text = "\( c * 100)%"
    }
    
    func saveInLocals(_ stats:AddMoreResponse){
        UDM.shared.saveString(stats.bw22_total_balance.withCommas(), Constants.userTotalBalance)
        UDM.shared.saveString(stats.user_usdt_totoal_balance.withCommas(), Constants.userUSDT)
        UDM.shared.saveString("\(stats.change_in_24_hours_percentage)%", Constants.changeInPercentage)
        UDM.shared.saveString("\(stats.usdt_value)", Constants.usdt)
        UDM.shared.saveString(stats.market_total_pre_booking_value.withCommas(), Constants.prebooking)
        UDM.shared.saveString(stats.market_cap_usdt_value.withCommas(), Constants.marketCap)
        UDM.shared.saveBool(stats.is_up, Constants.isUP)
        UDM.shared.saveInt(stats.total_pre_booked_amount, Constants.BookedAmount)
    }
    func saveInLocals(_ stats:UserStatsResponse){
        UDM.shared.saveString(stats.bw22_total_balance.withCommas(), Constants.userTotalBalance)
        UDM.shared.saveString(stats.user_usdt_totoal_balance.withCommas(), Constants.userUSDT)
        UDM.shared.saveString("\(stats.change_in_24_hours_percentage)%", Constants.changeInPercentage)
        UDM.shared.saveString("\(stats.usdt_value)", Constants.usdt)
        UDM.shared.saveString(stats.market_total_pre_booking_value.withCommas(), Constants.prebooking)
        UDM.shared.saveString(stats.market_cap_usdt_value.withCommas(), Constants.marketCap)
        UDM.shared.saveBool(stats.is_up, Constants.isUP)
        UDM.shared.saveInt(stats.total_pre_booked_amount, Constants.BookedAmount)
    }
    
    func handleUserStatsSuccess (_ stats: UserStatsResponse){
        self.txtTotalBalance.text = stats.bw22_total_balance.withCommas()
        self.txtUsdt.text = "\(stats.user_usdt_totoal_balance.withCommas())"
        self.change_percentage.text = "\(stats.change_in_24_hours_percentage)%"
        self.txtUsdtValue.text = "\(stats.usdt_value)"
        self.txtPreBooking.text = "\(stats.market_total_pre_booking_value.withCommas())"
        self.txtMarketCap.text = "\(stats.market_cap_usdt_value.withCommas())"
        print(".......... used blanace    \(stats.total_pre_booked_amount)")
        
        
        // do for progressbar
        let a = Float(stats.total_pre_booked_amount)
        let b = Float(1000)
        let c = a / b
        CircularProgress.setProgressWithAnimation(duration: 0.0, value: c)
        txtPercentage.text = "\( round(c * 100))%"
   
        
        if(stats.is_up){
            self.arrow.image = UIImage(named: "Vectorup")
            self.change_percentage.textColor = UIColor(named: "greenArrowColor")
        }else{
            self.arrow.image = UIImage(named: "Vectordown")
            self.change_percentage.textColor = UIColor(named: "redArrowColor")
        }
        saveInLocals(stats)
    }
        func showLoading(){
        let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
            
            
        }
        
        func hideLoading(){
        
            self.dismiss(animated: false, completion: nil)
        }
    
    
         func showSnackBar(_ message:String){
             MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
         }


}



extension HomeViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
    }
}




extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}



