//
//  NewsViewController.swift
//  BW22
//
//  Created by PSE on 20/04/2022.
//

import UIKit
import WebKit

class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var ivNotification: UIImageView!
    @IBOutlet weak var table: UITableView!
    
    var detailsList : [String] = []
    var imagesList : [String] = []
    let data = ["New York, NY", "Los Angeles, CA", "Chicago, IL", "Houston, TX",
        "Philadelphia, PA", "Phoenix, AZ", "San Diego, CA", "San Antonio, TX",
        "Dallas, TX", "Detroit, MI", "San Jose, CA", "Indianapolis, IN",
        "Jacksonville, FL", "San Francisco, CA", "Columbus, OH", "Austin, TX",
        "Memphis, TN", "Baltimore, MD", "Charlotte, ND", "Fort Worth, TX"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.dataSource = self
        self.table.delegate = self
        
        self.showLoading()
        URLSession.shared.request(url: URL(string: Constants.GetNews), model: WrappedResponse<NewsResponse>.self){
            resulst in
            DispatchQueue.main.async {
                switch resulst {
                case .success(let response):
                    self.handleNewsData(response)
                case .failure(let error):
                    self.handleNewsFailure(error)
                }
            }
        }

    }
    
    
    func handleNewsData(_ response : NewsResponse){
        self.hideLoading()
    
         imagesList = response.news.map{ return $0.key }
        detailsList = response.news.map{ return $0.value }
        
        table.reloadData()
        print(detailsList)
    }
    

func handleNewsFailure(_ error:Error){
    self.hideLoading()
    switch error {
    case CustomError.invalidBodyJson:
        MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
        
    case CustomError.invalidURL:
        MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
    
    case CustomError.invalidData:
        MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
    
    case is ApiError:
        let err = error as! ApiError
        MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
    default:
        MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

    }
    
}
    
    func showLoading(){
    let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    loadingIndicator.style = UIActivityIndicatorView.Style.medium
    loadingIndicator.startAnimating();

    alert.view.addSubview(loadingIndicator)
    present(alert, animated: true, completion: nil)
        
    }
    
    func hideLoading(){
    
        self.dismiss(animated: false, completion: nil)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "cell") as! CustomTableViewCell
        
        cell.txtDetails.text = data[indexPath.row]
      //  cell.ivDetails.load(imagesList[indexPath.row])
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
}


