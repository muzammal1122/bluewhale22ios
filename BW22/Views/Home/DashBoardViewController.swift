//
//  DashBoardViewController.swift
//  BW22
//
//  Created by PSE on 20/04/2022.
//

import UIKit
import Charts

class DashBoardViewController: UIViewController {
   

    @IBOutlet weak var lineChart: LineChartView!
    
    @IBOutlet weak var txtMarketPrice: UILabel!
    
    @IBOutlet weak var txtMarketPercentage: UILabel!
    
    var yValues = [ChartDataEntry]()
    var chartENtries : [String : Double] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     //   lineChart.delegate = self
     
        loadFromLocalsFirst()
        requestDashBoardData()
        
        // click on notification icon
        let gestureRecognizerI = UITapGestureRecognizer(target: self, action: #selector(showNotification))
            self.imgBack.addGestureRecognizer(gestureRecognizerI)
        
    }
   
    
    func setNewData(dates:[String],values:[Double]){
        lineChart.rightAxis.enabled = false
        let yAxis = lineChart.leftAxis
        yAxis.labelTextColor = .black
        yAxis.axisLineColor = .white

        lineChart.xAxis.labelTextColor = .black
        lineChart.xAxis.axisLineColor = .white
        lineChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
        lineChart.animate(xAxisDuration: 1.5)
        lineChart.setBarChartData(xValues: dates, yValues: values, label: "")
        
    }
    
    func requestDashBoardData(){
        let url = URL(string: Constants.RequestDashBoardData)
        
            URLSession.shared.request(url: url, model: WrappedResponse<DashBoardResponse>.self){
                result in
                DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.handleRequestURLSuccessResponse(response)
                case .failure(let error):
                    self.handleRequestURLFailureResponse(error)
                }
            }
        }
     
    
    }
    @IBOutlet weak var imgBack: UIImageView!
    
    func handleRequestURLSuccessResponse(_ response:DashBoardResponse){
        
        var dates: [String] = []
        var values: [Double] = []
        
        txtMarketPrice.text = "\(response.change_in_24_hours_market_cap_percentage)%"
        txtMarketPercentage.text = "\(response.market_cap_usdt_value.withCommas())"
        //print(response.market_movement)
        chartENtries  = response.market_movement
        for (key, value) in response.market_movement {
            
            
            print("Key: \(key) | value: \(value)")
            yValues.append(ChartDataEntry(x: value, y: value))
            dates.append(key)
            values.append(value)
            
        }
        print(yValues)
        
        setNewData(dates: dates, values: values)
        saveInLocals("\(response.change_in_24_hours_market_cap_percentage)%")
     
    }
    
    func saveInLocals (_ marketCapChangeIn24Hours:String){
        UDM.shared.saveString(marketCapChangeIn24Hours, Constants.preBookingChangeInLast24Hours)
    }
    func loadFromLocalsFirst (){
        let marketCap = UDM.shared.getStringValue(Constants.marketCap)
        let preBookingChangeInLast24Hours = UDM.shared.getStringValue(Constants.preBookingChangeInLast24Hours)
        txtMarketPrice.text = preBookingChangeInLast24Hours
        txtMarketPercentage.text = marketCap
        
    }
    
    
         func showSnackBar(_ message:String){
             MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
         }
    
    func handleRequestURLFailureResponse(_ error:Error){
        switch error {
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
    }
    
    @objc func showNotification(){
        showSnackBar("No Notifications found")
    }
    
    


}


extension LineChartView {
   

    private class BarChartFormatter: IndexAxisValueFormatter {
        
        var labels: [String] = []
        
        override func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            return labels[Int(value)]
        }
        
        init(labels: [String]) {
            super.init()
            self.labels = labels
        }
    }
    
    func setBarChartData(xValues: [String], yValues: [Double], label: String) {
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<yValues.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: yValues[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = LineChartDataSet(entries: dataEntries, label: label)
        chartDataSet.drawCirclesEnabled = true
        chartDataSet.mode = .cubicBezier
        chartDataSet.setColor(.black)
        chartDataSet.fill = ColorFill(color: .systemBlue)
        chartDataSet.fillAlpha = 125
        chartDataSet.drawFilledEnabled = true
        let chartData = LineChartData(dataSet: chartDataSet)
        
        let chartFormatter = BarChartFormatter(labels: xValues)
        let xAxis = XAxis()
        xAxis.valueFormatter = chartFormatter as? AxisValueFormatter
        self.xAxis.valueFormatter = xAxis.valueFormatter

        self.data = chartData
    }
}
