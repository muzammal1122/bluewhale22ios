//
//  MenuViewController.swift
//  BW22
//
//  Created by PSE on 20/04/2022.
//

import UIKit

import Firebase

class MenuViewController: UIViewController {
    
    @IBOutlet weak var ProfileStack: UIStackView!
    
    @IBOutlet weak var KycStack: UIStackView!
    
    @IBOutlet weak var ContactStack: UIStackView!
    
    @IBOutlet weak var PPStack: UIStackView!
    
    private let auth = Auth.auth()
    
    @IBOutlet weak var LogoutStack: UIStackView!
    
    @IBOutlet weak var imgBack: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // click on back icon
        let gestureRecognizerI = UITapGestureRecognizer(target: self, action: #selector(showNotification))
            self.imgBack.addGestureRecognizer(gestureRecognizerI)

        // click on profile
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAction))
            self.ProfileStack.addGestureRecognizer(gestureRecognizer)
        
        // click on KycStack
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(goToKyc))
            self.KycStack.addGestureRecognizer(gestureRecognizer1)
        
        // click on ContactStack
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(goToContact))
            self.ContactStack.addGestureRecognizer(gestureRecognizer2)
        
        
        // click on PPStack
        let gestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(goToPP))
            self.PPStack.addGestureRecognizer(gestureRecognizer4)
        
        // click on log out
        let gestureRecognizer5 = UITapGestureRecognizer(target: self, action: #selector(doLogOut))
            self.LogoutStack.addGestureRecognizer(gestureRecognizer5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.txtName.text = UDM.shared.getStringValue(Constants.firstName)+" "+UDM.shared.getStringValue(Constants.lastName)
 //       self.txtEmail.text = UDM.shared.getStringValue(Constants.email)
      
    }

     func showActionSheet(){
         let alert = UIAlertController(title: "confirmation".localized, message: "in_order_to_forgot".localized, preferredStyle: .alert)

         alert.addAction(UIAlertAction(title: "confirm".localized, style: .default){
            handler in
            
            do{
                try self.auth.signOut()
                UDM.shared.saveBool(true,Constants.isUserFirstTimeInstalledApp)
                UDM.shared.saveString("nil", Constants.userAccountStatus)
                UDM.shared.saveString("nil", Constants.referralCode)
                UDM.shared.saveString("nil", Constants.email)
                UDM.shared.saveString("nil", Constants.firstName)
                UDM.shared.saveString("nil", Constants.lastName)
                UDM.shared.saveString("nil", Constants.appSecureCredentials)
                self.startLoginVC()
            }
            catch {
                self.showSnackBar(error.localizedDescription)
            }
            
        })
        
         alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel){
            handler in
        })

         self.present(alert, animated: true, completion: nil)
         
//         if let popoverPresentationController = alert.popoverPresentationController {
//             popoverPresentationController.sourceView = self.view
//             popoverPresentationController.sourceRect = CGRect(x: self.view.frame.midX, y: self.view.frame.midY, width: 0, height: 0)
//         }
//         self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @objc func showNotification(){
        showSnackBar("No Notifications found")
    }
    
    @objc func tapAction() -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func goToKyc() -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.url = "https://www.bluewhale.host/kyc.php"
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    @objc func goToContact() -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @objc func goToPP() -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.url = "https://www.iubenda.com/privacy-policy/63112702"
        self.present(vc, animated: true, completion: nil)
    }
    

    func startLoginVC() {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
    }
    
    
    @objc func doLogOut(){
        self.showActionSheet()
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }

}
