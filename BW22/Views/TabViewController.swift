//
//  TabViewController.swift
//  BW22
//
//  Created by PSE on 20/04/2022.
//

import UIKit

class TabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
       // initializeHideKeyboard()
        if #available(iOS 15.0, *) {
           let appearance = UITabBarAppearance()
           let itemAppearance = UITabBarItemAppearance()
            
            itemAppearance.selected.iconColor = UIColor(named: "DarkBlue")
            itemAppearance.normal.iconColor = UIColor(named: "normalIconColor")
            itemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "normalIconColor") ?? ""]
            itemAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "DarkBlue") ?? ""]
            
            appearance.configureWithOpaqueBackground()
          //  appearance.backgroundImage = UIImage(named: "bottomNav")
            
            appearance.stackedLayoutAppearance = itemAppearance
           
           self.tabBar.standardAppearance = appearance
            self.tabBar.scrollEdgeAppearance = self.tabBar.standardAppearance
        }else{
        
            
            
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}

extension TabViewController {
 func initializeHideKeyboard(){
 //Declare a Tap Gesture Recognizer which will trigger our dismissMyKeyboard() function
 let tap: UITapGestureRecognizer = UITapGestureRecognizer(
 target: self,
 action: #selector(dismissMyKeyboard))
 //Add this tap gesture recognizer to the parent view
 view.addGestureRecognizer(tap)
 }
 @objc func dismissMyKeyboard(){
 //endEditing causes the view (or one of its embedded text fields) to resign the first responder status.
 //In short- Dismiss the active keyboard.
 view.endEditing(true)
 }
 }

