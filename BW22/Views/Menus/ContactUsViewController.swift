//
//  ContactUsViewController.swift
//  BW22
//
//  Created by PSE on 21/04/2022.
//

import UIKit

class ContactUsViewController: UIViewController, UITextViewDelegate {
    
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var etEmail: UITextField!{
        didSet{
            etEmail.layer.cornerRadius = 5
            etEmail.layer.shadowOpacity = 0.7
            etEmail.layer.shadowOffset = CGSize(width: 3, height: 3)
            etEmail.layer.shadowRadius = 15.0
            etEmail.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var txtMessage: UITextView!
    
    @IBOutlet weak var uiTextView: UIView!{
        didSet{
            txtMessage.layer.cornerRadius = 5
            txtMessage.layer.shadowOpacity = 0.7
            txtMessage.layer.shadowOffset = CGSize(width: 3, height: 3)
            txtMessage.layer.shadowRadius = 15.0
            txtMessage.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var counter: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        etEmail.maxLength = 10
        txtMessage.delegate = self
        

        // click on back icon
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToBack))
            self.imgBack.addGestureRecognizer(gestureRecognizer)
    }
    

    @IBAction func btnSend(_ sender: Any) {
        
        if(!validate(textView: etEmail)){
           
        } else if (txtMessage.text.isEmpty){
            showSnackBar("please_enter_message".localized)
        }else{
            sendEmail(body: EmailBody(email: etEmail.text!, body: txtMessage.text))
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        counter.text = "\(numberOfChars)/300"
        print(numberOfChars)
        return numberOfChars < 300    // 10 Limit Value
    }
    
    @objc func goToBack() -> Void {
        self.dismiss(animated: true, completion: nil)
    }

    
    func validate(textView: UITextField) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
            showSnackBar("please_enter_email".localized)
            return false
        }
        
        let withText = textView.text!
        if(!withText.contains("@")){
            showSnackBar("Email is badly formatted".localized)
            return false
        }

        return true
    }
    
    
    func sendEmail(body:EmailBody){
        self.showLoading()
        URLSession.shared.requestUpload(url: Constants.SendEmailURL, type: "POST", body: body, model: WrappedResponse<EmailResponse>.self){
            result in
            DispatchQueue.main.async{
                switch result {
                case .success(let response):
                    self.handleSubscribeReferralCodeSuccess(response)
                case .failure(let error):
                    self.handleSubscribeReferralCodeFailure(error)
                }
            }
        }
    }
    
    func handleSubscribeReferralCodeSuccess(_ response:EmailResponse){
        showSnackBar(response.message)
        self.hideLoading()
    
    }
    func handleSubscribeReferralCodeFailure(_ error:Error){
        self.hideLoading()
        
        switch error {
        case CustomError.invalidBodyJson:
            MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
            
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
    }
    
    
    
    func showLoading(){
        let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        
    }
    
    func hideLoading(){
    
        self.dismiss(animated: false, completion: nil)
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
               return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = String((t!.prefix(maxLength)))
    }
}
