//
//  WebViewViewController.swift
//  BW22
//
//  Created by PSE on 21/04/2022.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {
    
    var url : String = "https://www.bluewhale.host/main.php"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: url)!
        myWebView.load(URLRequest(url: url))
        myWebView.allowsBackForwardNavigationGestures = true
    }
    

    @IBOutlet weak var myWebView: WKWebView!
}

    extension WebViewViewController : WKNavigationDelegate {
        
        override func loadView() {
            myWebView = WKWebView()
            myWebView.navigationDelegate = self
            view = myWebView 
        }
    }



