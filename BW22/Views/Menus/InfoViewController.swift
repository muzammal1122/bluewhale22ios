//
//  InfoViewController.swift
//  BW22
//
//  Created by PSE on 21/04/2022.
//

import UIKit

class InfoViewController: UIViewController {
    
    
    @IBAction func btnGOTIT(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBOutlet weak var dialogBoxView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //adding an overlay to the view to give focus to the dialog box
        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
    }
    


//MARK:- functions for the viewController
static func showPopup(parentVC: UIViewController){
    print("alert")
    //creating a reference for the dialogView controller
    if let popupViewController = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "InfoViewController") as? InfoViewController {
    popupViewController.modalPresentationStyle = .custom
    popupViewController.modalTransitionStyle = .crossDissolve
        parentVC.present(popupViewController, animated: true)
    
    }
  }
}


