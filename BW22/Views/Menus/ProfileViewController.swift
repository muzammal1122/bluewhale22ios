//
//  ProfileViewController.swift
//  BW22
//
//  Created by PSE on 21/04/2022.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    @IBOutlet weak var etFirstName: UITextField!{
        didSet{
            etFirstName.layer.cornerRadius = 5
            etFirstName.layer.shadowOpacity = 0.7
            etFirstName.layer.shadowOffset = CGSize(width: 3, height: 3)
            etFirstName.layer.shadowRadius = 15.0
            etFirstName.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var FirestLetter: UILabel!
    @IBOutlet weak var nameView: UIView!{
        didSet{
            nameView.layer.masksToBounds = true
            nameView.layer.cornerRadius = 37
            
        }
    }
    
    @IBOutlet weak var etLastName: UITextField!{
        didSet{
            etLastName.layer.cornerRadius = 5
            etLastName.layer.shadowOpacity = 0.7
            etLastName.layer.shadowOffset = CGSize(width: 3, height: 3)
            etLastName.layer.shadowRadius = 15.0
            etLastName.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var etEmail: UITextField!{
        didSet{
            etEmail.layer.cornerRadius = 5
            etEmail.layer.shadowOpacity = 0.7
            etEmail.layer.shadowOffset = CGSize(width: 3, height: 3)
            etEmail.layer.shadowRadius = 15.0
            etEmail.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var etPhone: UITextField!{
        didSet{
            etPhone.layer.cornerRadius = 5
            etPhone.layer.shadowOpacity = 0.7
            etPhone.layer.shadowOffset = CGSize(width: 3, height: 3)
            etPhone.layer.shadowRadius = 15.0
            etPhone.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var imgBack: UIImageView!
    
    var body : ProfileBody?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("view did load")
        
        hideKeyboardWhenTappedAround()
        
        etEmail.isEnabled = false
        etPhone.isEnabled = false
        let name : String = (UDM.shared.getStringValue(Constants.firstName))
        etFirstName.text = name
    
        if let c = name.first {
            FirestLetter.text = String(c)
        }
        
        etLastName.text = UDM.shared.getStringValue(Constants.lastName)
        etEmail.text = UDM.shared.getStringValue(Constants.email)
        etPhone.text = UDM.shared.getStringValue(Constants.phoneNumber)
    

        // click on back icon
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToBack))
            self.imgBack.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("view did appear")
        print(UDM.shared.getStringValue(Constants.lastName))
    }
    

    @IBAction func btnUpdate(_ sender: UIButton) {
        if(validate(textView: etFirstName) && validate(textView: etLastName)){
            print("update")
            body = ProfileBody(first_name: etFirstName.text!, last_name: etLastName.text!, address: "Duabi")
            UpdateUserProfile(body: body!)
        }else{
            // show message
            showSnackBar("Please enter name")
        }
    }
    
    
    @objc func goToBack() -> Void {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validate(textView: UITextField) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
            // this will be reached if the text is nil (unlikely)
            // or if the text only contains white spaces
            // or no text at all
            return false
        }

        return true
    }
    
    
    func UpdateUserProfile(body:ProfileBody){
        self.showLoading()
        URLSession.shared.requestUpload(url: URLRequest(url: (URL(string: Constants.RequestProfileUpdate + UDM.shared.getStringValue(Constants.phoneNumber)))!), type: "PUT", body: body, model: WrappedResponse<ProfileResponse>.self){
            result in
            DispatchQueue.main.async{
                switch result {
                case .success(let response):
                    self.handleSubscribeReferralCodeSuccess(response)
                case .failure(let error):
                    self.handleSubscribeReferralCodeFailure(error)
                }
            }
        }
    }
    
    func handleSubscribeReferralCodeSuccess(_ response:ProfileResponse){
        print(body!.last_name)
        UDM.shared.saveString(body!.first_name, Constants.firstName)
        UDM.shared.saveString(body!.last_name, Constants.lastName)
        
        showSnackBar(response.message)
        self.hideLoading()
        print(UDM.shared.getStringValue(Constants.lastName))
    }
    func handleSubscribeReferralCodeFailure(_ error:Error){
        self.hideLoading()
        
        switch error {
        case CustomError.invalidBodyJson:
            MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
            
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
    }
    
    
    
    func showLoading(){
        let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        
    }
    
    func hideLoading(){
    
        self.dismiss(animated: false, completion: nil)
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }

}
