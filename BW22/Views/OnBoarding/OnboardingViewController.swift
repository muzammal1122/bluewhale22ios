//
//  OnboardingViewController.swift
//  BW22
//
//  Created by user on 19/04/2022.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var imgNext: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // click on profile
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAction))
            self.imgNext.addGestureRecognizer(gestureRecognizer)
    
}
    
    @objc func tapAction() -> Void {
        print("hello yolo")
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
    }

    
}


