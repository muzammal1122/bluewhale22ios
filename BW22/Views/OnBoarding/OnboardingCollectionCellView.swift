//
//  OnboardingCollectionCellCollectionViewCell.swift
//  BW22
//
//  Created by user on 19/04/2022.
//

import UIKit

class OnboardingCollectionCellView: UICollectionViewCell {
    
    static let identifier = String (describing: OnboardingCollectionCellView.self)
    @IBOutlet weak var slideImageView: UIImageView!
    
    
    func setup (_ slide:WalkThroughModel){

        slideImageView.image = UIImage.init(named: slide.image)
    }
}

