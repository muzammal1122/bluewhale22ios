//
//  RegistrationViewController.swift
//  BW22
//
//  Created by user on 20/04/2022.
//

import UIKit

class RegistrationViewController: UIViewController {

    
    @IBOutlet weak var firstName: UITextField!{
        didSet{
            firstName.layer.cornerRadius = 5
            firstName.layer.shadowOpacity = 0.7
            firstName.layer.shadowOffset = CGSize(width: 3, height: 3)
            firstName.layer.shadowRadius = 15.0
            firstName.layer.shadowColor = UIColor.darkGray.cgColor
        }
    }
    @IBOutlet weak var lastName: UITextField!{
        didSet{
            lastName.layer.cornerRadius = 5
            lastName.layer.shadowOpacity = 0.7
            lastName.layer.shadowOffset = CGSize(width: 3, height: 3)
            lastName.layer.shadowRadius = 15.0
            lastName.layer.shadowColor = UIColor.darkGray.cgColor
        }
    }
    @IBOutlet weak var emailTF: UITextField!{
        didSet{
            emailTF.layer.cornerRadius = 5
            emailTF.layer.shadowOpacity = 0.7
            emailTF.layer.shadowOffset = CGSize(width: 3, height: 3)
            emailTF.layer.shadowRadius = 15.0
            emailTF.layer.shadowColor = UIColor.darkGray.cgColor
        }
    }
    

    var phoneNumber:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    
    func registerUser(){
        let mFirstName = self.firstName.text
        let mLastName = self.lastName.text
        let mEmail = self.emailTF.text
        
        if(mFirstName?.isEmpty ??  true || mFirstName == nil){
            MySnack.make(in: self.view, message: "please_enter_first_name".localized, duration: .lengthShort).show()

        }
        else if (mLastName?.isEmpty ??  true || mLastName == nil){
            MySnack.make(in: self.view, message: "please_enter_last_name".localized, duration: .lengthShort).show()

        }
        else if (mEmail?.isEmpty ??  true || mEmail == nil){
            MySnack.make(in: self.view, message: "please_enter_email".localized, duration: .lengthShort).show()

        }
        
        else if (!mEmail!.contains("@")){
            MySnack.make(in: self.view, message: "email_is_wrong_formated".localized, duration: .lengthShort).show()
        }
        else {
           
            let phone = UDM.shared.getStringValue(Constants.phoneNumber)
            if(phone == "nil"){
                MySnack.make(in: self.view, message: "phone_number_missing".localized, duration: .lengthShort).show()
                return
            }


                let userRegistrationBody = UserRegistrationBody(firstName: mFirstName!, lastName: mLastName!, email: mEmail!, phoneNumber: phone, notificationToken: "not_set", image: "")
                let urlRequest = Constants.UserRegisrationURLRequest
            self.showLoading()
            URLSession.shared.requestUpload(url: urlRequest, type: "POST", body: userRegistrationBody, model: WrappedResponse<UserRegistrationResponse>.self){result in
                    DispatchQueue.main.async {
                        self.hideLoading()
                        switch result {
                        case .success(let userResponse):
                            UDM.shared.saveString(mEmail!, Constants.email)
                            UDM.shared.saveString(mFirstName!, Constants.firstName)
                            UDM.shared.saveString(mLastName!, Constants.lastName)
                            self.handleUserRegistrationSuccess(userResponse)
                        case .failure(let error):
                            self.handleUserRegisrationFailure(error)
                        }
                    }
                  
                
                    
                }
            
           
                                            
        }
    }

    @IBAction func nextButton(_ sender: Any) {
        registerUser()
    }
    
    func handleUserRegistrationSuccess(_ response:UserRegistrationResponse){
        
        UDM.shared.saveString(Constants.shouldGetReferralCode, Constants.userAccountStatus)
        UDM.shared.saveString(response.referral_code, Constants.referralCode)
      
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferralCodeViewController") as! ReferralCodeViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
        
        
    }
    func handleUserRegisrationFailure(_ error:Error){
        

            switch error {
            case CustomError.invalidBodyJson:
                MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
                
            case CustomError.invalidURL:
                MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
            
            case CustomError.invalidData:
                MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
            
            case is ApiError:
                let err = error as! ApiError
                MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
                
            default:
                MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

            }
        
      
    }

    func showLoading(){
        let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func hideLoading(){

        dismiss(animated: false, completion: nil)
    }


}
