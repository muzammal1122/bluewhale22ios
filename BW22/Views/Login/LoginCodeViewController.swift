//
//  LoginCodeViewController.swift
//  BW22
//
//  Created by user on 20/04/2022.
//

import UIKit
import DPOTPView
import Firebase
class LoginCodeViewController: UIViewController, DPOTPViewDelegate {
    
    @IBOutlet weak var txtOTPView: DPOTPView!
    @IBOutlet weak var enterLoginCodeLabel: UILabel!
    
    @IBOutlet weak var invalidCodeLabel: UILabel!
    
    @IBOutlet weak var forgetLoginCodeLabel: UILabel!
    
    var code:String?;
    
    let auth = Auth.auth()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtOTPView.dpOTPViewDelegate = self
        hideKeyboardWhenTappedAround()
        txtOTPView.count = 4
        txtOTPView.spacing  = 20
        txtOTPView.fontTextField = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(25.0))!
        txtOTPView.dismissOnLastEntry = true
    
    
        txtOTPView.borderWidthTextField = 2
        txtOTPView.backGroundColorTextField = .white
        txtOTPView.cornerRadiusTextField = 8
        txtOTPView.isCursorHidden = true
        
        self.forgetLoginCodeLabel.text = "forgot_login_code".localized
        
        let gestureRecog = UITapGestureRecognizer(target: self, action: #selector(forgotLoginCodeTap))
        self.forgetLoginCodeLabel.addGestureRecognizer(gestureRecog)
     
    }
    
    @objc
    func forgotLoginCodeTap() -> Void {
        print("clicked")
        self.showActionSheet()
    }
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        
        let code  = UDM.shared.getStringValue(Constants.appSecureCredentials)
        
        
        
        if(text.count == 4){
        if(txtOTPView.text == code){
            let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
            secondVC.modalPresentationStyle = .custom
            self.present(secondVC, animated: true, completion: nil)
        }
        else {
            invalidCodeLabel.isHidden = false
        }
            
        }
        else {
            invalidCodeLabel.isHidden = true
        }
//

     
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
       
    }
    
    func dpOTPViewBecomeFirstResponder() {
      
    }
    
    func dpOTPViewResignFirstResponder() {
       
    }
    func showActionSheet(){
        let alert = UIAlertController(title: "confirmation".localized, message:"in_order_to_forgot".localized , preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "confirm".localized, style: .default){
            handler in
            
            do{
                try self.auth.signOut()
                UDM.shared.saveBool(true,Constants.isUserFirstTimeInstalledApp)
                UDM.shared.saveString("nil", Constants.userAccountStatus)
                UDM.shared.saveString("nil", Constants.referralCode)
                UDM.shared.saveString("nil", Constants.email)
                UDM.shared.saveString("nil", Constants.firstName)
                UDM.shared.saveString("nil", Constants.lastName)
                UDM.shared.saveString("nil", Constants.appSecureCredentials)
                self.startLoginVC()
            }
            catch {
                self.showSnackBar(error.localizedDescription)
            }
            
        })
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel){
            handler in
        })

        present(alert, animated: true, completion: nil)
        
        
    }
    
    func startLoginVC() {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }


}
