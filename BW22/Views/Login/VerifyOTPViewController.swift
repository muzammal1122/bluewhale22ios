//
//  VerifyOTPViewController.swift
//  BW22
//
//  Created by user on 20/04/2022.
//

import UIKit
import DPOTPView
import Firebase


class VerifyOTPViewController: UIViewController,DPOTPViewDelegate {
    
    @IBOutlet weak var resendLabel: UIImageView!
   
    @IBOutlet weak var resendCounter: UILabel!
    
    @IBOutlet weak var mobileNumberLabel: UILabel!
    var secondsRemaining = 60
    var phoneNumber: String?
    var resendingID: String?
    var notificationToken: String?
        
    
    @IBOutlet weak var txtOTPView: DPOTPView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        txtOTPView.dpOTPViewDelegate = self
        setupOTPView()
        self.startCounter()
        
        // click on resend
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sendCode))
            self.resendCounter.addGestureRecognizer(gestureRecognizer)
  
    }
    
    
    
    
    @objc func sendCode() -> Void {
        print("send code  ")
        if (phoneNumber != nil){
        PhoneAuthProvider.provider()
          .verifyPhoneNumber("+" + phoneNumber!, uiDelegate: nil) { verificationID, error in
              if let error = error {
                  self.showSnackBar(error.localizedDescription)
                return
              }
          }
            
          }
    }
    
    
    func setupOTPView (){
        txtOTPView.count = 6
        txtOTPView.spacing = 10
        txtOTPView.fontTextField = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(25.0))!
        txtOTPView.dismissOnLastEntry = true
    
    
        txtOTPView.borderWidthTextField = 2
        txtOTPView.backGroundColorTextField = .white
        txtOTPView.cornerRadiusTextField = 8
        txtOTPView.isCursorHidden = true

        mobileNumberLabel.text = "We have sent you 6-digit code on your mobile number:".localized+" +\(phoneNumber ?? "********")"
    }
    
    
    
    func startCounter(){
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
              if self.secondsRemaining > 0 {
                  if(self.secondsRemaining < 20){
                      self.resendCounter.textColor = .red
                  }
                  self.resendCounter.text = "Resend verfication code in".localized+" \(self.secondsRemaining)s"
                  self.secondsRemaining -= 1
              } else {
                  self.resendCounter.textColor = .black
                  self.resendCounter.attributedText =  NSAttributedString(string: "Resend Code".localized, attributes:
                                                                                [.underlineStyle: NSUnderlineStyle.single.rawValue])
                  Timer.invalidate()
              }
          }
    }
    
    
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        
        if(text.count == 6){
            startVerifyingCode(text)
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
       
    }
    
    func dpOTPViewBecomeFirstResponder() {
      
    }
    
    func dpOTPViewResignFirstResponder() {
       
    }
    
    func startVerifyingCode(_ smsCode:String){
        PhoneAuthManager.shared.verifyCode(smsCode){
            result in
            
            switch result {
            case .failure(let error):
                
                let err = error as! AuthError
                MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
           
            default :
                self.handleSuccessVerifyCode();
                
            }
        
    }
    }
    
    func handleSuccessVerifyCode(){
        loginUser()
    }
    
    func loginUser(){
        notificationToken = "notification_token"
        guard let phoneNumber = phoneNumber else {
            showSnackBar("phone_number_missing".localized)
            return
        }
        guard let notificationToken = notificationToken else {
            showSnackBar("Notification token is missing".localized)
            return
        }
        
        let userLoginBody = UserLoginBody(phone_number: phoneNumber, notification_token: notificationToken)
        showLoading()
        URLSession.shared.requestUpload(url: Constants.UserLoginURL, type: "PUT", body: userLoginBody, model: WrappedResponse<UserLoginResponse>.self){
            result in
            DispatchQueue.main.async {
                self.hideLoading()
                switch result {

                case .success(let response):
                    self.handleUserLoginSuccessReponse(response)
                case .failure(let error):
                    self.handleUserLoginFailure(error)
                }
            }
            
        }
        
        
    }
    
    func handleUserLoginSuccessReponse(_ reponse:UserLoginResponse){
        // If user successfully login that means user already exist in the database
        // Now we don't need to register user, and, simply start the Login code screen
        UDM.shared.saveString(Constants.shouldSetLoginCode, Constants.userAccountStatus)
        UDM.shared.saveString((phoneNumber ?? "nil"), Constants.phoneNumber)
        UDM.shared.saveString(reponse.referral_code, Constants.referralCode)
        UDM.shared.saveString(reponse.email, Constants.email)
        UDM.shared.saveString(reponse.first_name, Constants.firstName)
        UDM.shared.saveString(reponse.last_name, Constants.lastName)
        
        startLoginCodeVC()
        
    }
    
    func handleUserLoginFailure(_ error:Error){
        

            switch error {
            case CustomError.invalidBodyJson:
                MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
                
            case CustomError.invalidURL:
                MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
            
            case CustomError.invalidData:
                MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
            
            
            case is ApiError:
                let err = error as! ApiError
                if err.errorMessage == "User not found" {
                    // If use not found then just simply register the User
                    UDM.shared.saveString(Constants.shouldRegister, Constants.userAccountStatus)
                    UDM.shared.saveString((phoneNumber ?? "nil"), Constants.phoneNumber)
                    startUserRegistrationVC()
                }else{
                    // In case any other error then just simply show the error
                    MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
                }
                
                
            default:
                MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

            }
        
      
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }
    
    func startUserRegistrationVC() {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        secondVC.modalPresentationStyle = .custom
        secondVC.phoneNumber = phoneNumber
        self.present(secondVC, animated: true, completion: nil)
    }
    
    func startLoginCodeVC() {
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
    }
    
    
    func showLoading(){
        let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        
    }
    
    func hideLoading(){
    
        self.dismiss(animated: false, completion: nil)
    }

}
