//
//  ReferralCodeViewController.swift
//  BW22
//
//  Created by user on 25/04/2022.
//

import UIKit

class ReferralCodeViewController: UIViewController {

    @IBOutlet weak var referralCodeTF: UITextField!
    
    @IBOutlet weak var requestReferralCodeLabel: UILabel! {
        didSet{
            self.requestReferralCodeLabel.attributedText = NSAttributedString(string: "request_for_referral_code".localized, attributes:
                                                                          [.underlineStyle: NSUnderlineStyle.single.rawValue])
            self.requestReferralCodeLabel.textColor = .white
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onClickRequestReferralCode))
                 requestReferralCodeLabel.isUserInteractionEnabled = true
                 requestReferralCodeLabel.addGestureRecognizer(tap)
            
            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    
    @objc
       func onClickRequestReferralCode(sender:UITapGestureRecognizer) {
           var phoneNumber = UDM.shared.getStringValue(Constants.phoneNumber)
          // phoneNumber = "user"
           if(phoneNumber == "nil"){
               showSnackBar("phone_number_is_missing".localized)
               return
           }
           showLoading()
           print("PhoneNumber: \(phoneNumber)")
           requestReferralCode(phoneNumber: phoneNumber)
       }
    
    @IBAction func nextButton(_ sender: Any) {
        let referralCodeText = referralCodeTF.text
        if(referralCodeText == "" ){
            showSnackBar("please_enter_referral_code".localized)
            return
        }
        
        var phoneNumber = UDM.shared.getStringValue(Constants.phoneNumber)
        phoneNumber = "user"
        if(phoneNumber == "nil"){
            showSnackBar("phone_number_is_missing".localized)
            return
        }
        showLoading()
        subscribeReferralCode(body: SubscribeUserReferralDetailsBody(referral_code: referralCodeText!, phone_number: phoneNumber))
        
        
        
    }
    
    func subscribeReferralCode(body:SubscribeUserReferralDetailsBody){
        URLSession.shared.requestUpload(url: Constants.SubscribeReferralURL, type: "PUT", body: body, model: WrappedResponse<SubscribeUserReferralDetailsResponse>.self){
            result in
            DispatchQueue.main.async{
                self.hideLoading()
                switch result {
                case .success(let response):
                    self.handleSubscribeReferralCodeSuccess(response)
                case .failure(let error):
                    self.handleSubscribeReferralCodeFailure(error)
                }
            }
        }
    }
    
    func handleSubscribeReferralCodeSuccess(_ response:SubscribeUserReferralDetailsResponse){
        UDM.shared.saveString(Constants.shouldSetLoginCode, Constants.userAccountStatus)
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
    }
    func handleSubscribeReferralCodeFailure(_ error:Error){
        switch error {
        case CustomError.invalidBodyJson:
            MySnack.make(in: self.view, message: "invalid_provided_json_body".localized, duration: .lengthShort).show()
            
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
    }
    
    
  
    
    func requestReferralCode(phoneNumber:String){
        let url = URL(string: Constants.RequestReferralCodeURL+phoneNumber)
        
            URLSession.shared.request(url: url, model: WrappedResponse<ReferralCodeResponse>.self){
                result in
                DispatchQueue.main.async {
                self.hideLoading()
                switch result {
                case .success(let response):
                    self.handleRequestURLSuccessResponse(response)
                case .failure(let error):
                    self.handleRequestURLFailureResponse(error)
                }
            }
        }
     
    
    }
    
    func handleRequestURLSuccessResponse(_ response:ReferralCodeResponse){
        referralCodeTF.text = response.referral_code
    }
    func handleRequestURLFailureResponse(_ error:Error){
        switch error {
        case CustomError.invalidURL:
            MySnack.make(in: self.view, message: "invalid_provided_URL".localized, duration: .lengthShort).show()
        
        case CustomError.invalidData:
            MySnack.make(in: self.view, message: "invalid_api_data_part".localized, duration: .lengthShort).show()
        
        case is ApiError:
            let err = error as! ApiError
            MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
        default:
            MySnack.make(in: self.view, message: error.localizedDescription, duration: .lengthShort).show()

        }
    }
    
    
    
    func showLoading(){
        let alert = UIAlertController(title: nil, message: "please_wait".localized, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        
    }
    
    func hideLoading(){
    
        self.dismiss(animated: false, completion: nil)
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }

}
