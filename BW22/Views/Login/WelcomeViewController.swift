//
//  WelcomeViewController.swift
//  BW22
//
//  Created by PSE on 13/06/2022.
//

import Foundation
import UIKit

class WelcomeViewController : UIViewController {
    
    
    @IBOutlet weak var txtName: UILabel!
    
    @IBAction func btnNext(_ sender: Any) {
        
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        secondVC.modalPresentationStyle = .custom
        self.present(secondVC, animated: true, completion: nil)
    }
    override func viewDidLoad(){
        txtName.text =  UDM.shared.getStringValue(Constants.firstName)
    }
}
