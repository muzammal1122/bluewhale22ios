//
//  LoginViewController.swift
//  BW22
//
//  Created by user on 19/04/2022.
//

import UIKit
import CountryPickerView
import DPOTPView
import SnackBar

class LoginViewController: UIViewController, CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
     
        self.phoneCode = country.phoneCode
    }
    
   
    
    @IBOutlet weak var phoneNumberTF: UITextField! {
        didSet{
            phoneNumberTF.layer.shadowOpacity = 0.7
            phoneNumberTF.layer.shadowOffset = CGSize(width: 3, height: 3)
            phoneNumberTF.layer.shadowRadius = 15.0
            phoneNumberTF.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    }
    
    @IBOutlet weak var countryPicker: CountryPickerView!
    
    private var phoneCode:String?

    
    @IBOutlet weak var locationView: UIView! {
        didSet{
            locationView.layer.cornerRadius = 5
            locationView.layer.shadowOpacity = 0.7
            locationView.layer.shadowOffset = CGSize(width: 3, height: 3)
            locationView.layer.shadowRadius = 15.0
            locationView.layer.shadowColor = UIColor(named: "DarkBlue")?.cgColor
        }
    } 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        countryPicker.delegate = self
        phoneCode = countryPicker?.selectedCountry.phoneCode
        
        UDM.shared.saveBool(true,Constants.isUserFirstTimeInstalledApp)
        print("isUserFirstTimeInstalledApp: \(UDM.shared.getBoolValue(Constants.isUserFirstTimeInstalledApp)) ")
        

    }
    
    @IBAction func nextButton(_ sender: Any) {
        
        var phoneNumber = phoneNumberTF.text
        
        
        if (phoneNumber?.isEmpty ?? true || phoneNumber == nil){
            MySnack.make(in: self.view, message: "please_enter_phone_number".localized, duration: .lengthShort).show()
        }else{
            
            guard let phonecode = phoneCode else {
                MySnack.make(in: self.view, message: "please_reselect_country".localized, duration: .lengthShort).show()
                return
            }
            
            let phoneCodeWithoutPlus = String(phonecode.dropFirst())
            if(phoneNumber!.starts(with: "0")){
                phoneNumber = String(phoneNumber!.dropFirst())
            }

            let fullPhoneNumber = phoneCodeWithoutPlus+phoneNumber!
            print("PhoneNumber: " + fullPhoneNumber)
            self.startPhoneAuthWithNumber("+"+fullPhoneNumber)

        }
        
    
    }
    
    
    func startPhoneAuthWithNumber(_ phoneNumber:String){
        PhoneAuthManager.shared.startAuth(phoneNumber){
            result in
            
            switch result {
            case .failure(let error):
            
                let err = error as! AuthError
                
                MySnack.make(in: self.view, message: err.errorMessage, duration: .lengthShort).show()
           
            case .success(let resendingID) :
                
                self.handleSuccessPhoneAuth(resendingID,phoneNumber);
                
            }
        
    }
    
}
    
    func handleSuccessPhoneAuth(_  resendingID: String, _ phoneNumber:  String){
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
        secondVC.modalPresentationStyle = .custom
        secondVC.phoneNumber = String(phoneNumber.dropFirst())
        secondVC.resendingID = resendingID
        self.present(secondVC, animated: true, completion: nil)
    }
    
    
}
