//
//  ConfirmViewController.swift
//  BW22
//
//  Created by user on 26/04/2022.
//

import UIKit
import DPOTPView

class ConfirmViewController: UIViewController, DPOTPViewDelegate {

    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var otpText: DPOTPView!
    
    @IBOutlet weak var backIcon: UIImageView!
    
    
    var code:String?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otpText.dpOTPViewDelegate = self
        hideKeyboardWhenTappedAround()
        otpText.count = 4
        otpText.spacing  = 20
        otpText.fontTextField = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(25.0))!
        otpText.dismissOnLastEntry = true
    
    
        otpText.borderWidthTextField = 2
        otpText.backGroundColorTextField = .white
        otpText.cornerRadiusTextField = 8
        otpText.isCursorHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onClickBackIcon))
        backIcon.isUserInteractionEnabled = true
        backIcon.addGestureRecognizer(tap)


    }
    
    @objc
       func onClickBackIcon(sender:UITapGestureRecognizer) {
           let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
           secondVC.modalPresentationStyle = .custom
           self.present(secondVC, animated: true, completion: nil)
       }
    
    @IBAction func confirmButton(_ sender: Any) {
        guard let code = code else {
            showSnackBar("code_is_missing".localized)
            return
        }
        
        if(code == otpText.text){
         UDM.shared.saveString(Constants.loggedIn, Constants.userAccountStatus)
            UDM.shared.saveString(code, Constants.appSecureCredentials)
         let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
         secondVC.modalPresentationStyle = .custom
         self.present(secondVC, animated: true, completion: nil)
        }
        
        else{
            showSnackBar("code_not_matched".localized)
        }
    }
    func dpOTPViewAddText(_ text: String, at position: Int) {
        
        if(text.count == 4){
            self.confirmButton.isHidden = false
        }
        else {
            self.confirmButton.isHidden = true
        }
            
//

    
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
       
    }
    
    func dpOTPViewBecomeFirstResponder() {
      
    }
    
    func dpOTPViewResignFirstResponder() {
       
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }
    
    

}
