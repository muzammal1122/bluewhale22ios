//
//  FirstViewController.swift
//  BW22
//
//  Created by user on 26/04/2022.
//

import UIKit
import DPOTPView
class FirstViewController: UIViewController, DPOTPViewDelegate {


    @IBOutlet weak var otpText: DPOTPView!
   
    @IBOutlet weak var nextButtonOutlet: UIButton!
    
    
    @IBAction func nextButton(_ sender: Any) {
        
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmViewController") as! ConfirmViewController
        secondVC.modalPresentationStyle = .custom
        secondVC.code = self.otpText.text
        self.present(secondVC, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        otpText.dpOTPViewDelegate = self
        hideKeyboardWhenTappedAround()
        otpText.count = 4
        otpText.spacing  = 20
        otpText.fontTextField = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(25.0))!
        otpText.dismissOnLastEntry = true
    
    
        otpText.borderWidthTextField = 2
        otpText.backGroundColorTextField = .white
        otpText.cornerRadiusTextField = 8
        otpText.isCursorHidden = true
        
        otpText.isBottomLineTextField = true


        // Do any additional setup after loading the view.
    }
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        
        if(text.count == 4){
            nextButtonOutlet.isHidden = false
        }
        else {
            nextButtonOutlet.isHidden = true
        }
            
//

    
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
       
    }
    
    func dpOTPViewBecomeFirstResponder() {
      
    }
    
    func dpOTPViewResignFirstResponder() {
       
    }
    
    func showSnackBar(_ message:String){
        MySnack.make(in: self.view, message: message, duration: .lengthShort).show()
    }
    

}
