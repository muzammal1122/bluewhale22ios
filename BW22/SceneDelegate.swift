//
//  SceneDelegate.swift
//  BW22
//
//  Created by user on 19/04/2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func initVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            
        
        let viewController:UIViewController?;
        
        let isUserFirstTimeInstalledApp = UDM.shared.getBoolValue(Constants.isUserFirstTimeInstalledApp)
        let userAccountStatus = UDM.shared.getStringValue(Constants.userAccountStatus)
        
        print ("LaunchAction:  isUserFirstTimeInstalledApp: \(isUserFirstTimeInstalledApp )")
        print ("LaunchAction:  userAccountStatus: " + (userAccountStatus))
        
        // When user visit app for the very first time
        if !(isUserFirstTimeInstalledApp) {
           viewController =  storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        }
        
        
        // When User phone authentication completed
        else if (userAccountStatus) != "nil" {
            
            // When user authenticated but needs to be give 4-digit code
            if (userAccountStatus) == Constants.loggedIn {
                viewController =  storyboard.instantiateViewController(withIdentifier: "LoginCodeViewController") as! LoginCodeViewController
            }
            
            else if (userAccountStatus) == Constants.shouldRegister {
                // User needs to be registered first
                viewController =  storyboard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
            }
            
            else if (userAccountStatus) == Constants.shouldSetLoginCode {
                viewController =  storyboard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            }
            else {
                // User needs to be provide referral code
                viewController =  storyboard.instantiateViewController(withIdentifier: "ReferralCodeViewController") as! ReferralCodeViewController
            }
            
            
           
        }
        else {
            // When User enter app second time but did't yet authenticated
            viewController =  storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        }
    
            
//        viewController!.modalPresentationStyle = .custom
//        guard let window = window else {
//            print("Window not found")
//            return
//        }
//        guard let rootViewController = window.rootViewController else {
//            print("Root view controller not found")
//            return
//        }
//
//        rootViewController.present(viewController!, animated: true, completion: nil)
        print ("Executed Successully")
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let mWindow = windowScene?.windows.first
        
        guard let nWindow = mWindow else {
            print("Window not found")
            return
        }
        
        nWindow.rootViewController = viewController

    }
    
    func customVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let viewController =  storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        viewController.modalPresentationStyle = .custom
        guard let window = window else {
            print("Window not found")
            return
        }
        guard let rootViewController = window.rootViewController else {
            print("Root view controller not found")
            return
        }
            
        rootViewController.present(viewController, animated: true, completion: nil)
        print ("Executed Successully")
    }
    

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        initVC()
       // customVC()
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

