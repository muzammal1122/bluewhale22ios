//
//  UserStats.swift
//  BW22
//
//  Created by user on 26/04/2022.
//

import Foundation


struct UserStatsResponse: Codable {
    let bw22_total_balance: Int
    let user_usdt_totoal_balance: Int
    let usdt_value: Double
    let market_total_pre_booking_value: Int
    let market_cap_usdt_value: Int
    let change_in_24_hours_percentage: Double
    let is_up:Bool
    let total_pre_booked_amount : Int
}
