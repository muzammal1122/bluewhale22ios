//
//  News.swift
//  BW22
//
//  Created by PSE on 27/06/2022.
//

import Foundation

struct NewsResponse : Codable {
    
    let news: [String: String]
}
