//
//  AddMore.swift
//  BW22
//
//  Created by user on 27/04/2022.
//

import Foundation

struct AddMoreBody : Encodable {
    let booking_amount: String
    let phone_number:  String
}

struct AddMoreResponse: Codable {
    let bw22_total_balance: Int
    let user_usdt_totoal_balance: Int
    let usdt_value: Double
    let market_total_pre_booking_value: Int
    let market_cap_usdt_value: Int
    let change_in_24_hours_percentage: Double
    var is_up:Bool
    let total_pre_booked_amount : Int
}
