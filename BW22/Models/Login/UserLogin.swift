//
//  UserLogin.swift
//  BW22
//
//  Created by user on 23/04/2022.
//

import Foundation

struct UserLoginBody:Encodable {
    let phone_number: String
    let notification_token: String
    
//    init (phoneNumber:String, notificationToken:String){
//        self.notification_token = notificationToken
//        self.phone_number = phoneNumber
//    }
//    
//    enum CodingKeys: String, CodingKey {
//        case phone_number, notification_token
//    }
}

struct UserLoginResponse:Codable {
    let first_name: String
    let last_name: String
    let email: String
    let referral_code: String
}
