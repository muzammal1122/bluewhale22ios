//
//  UserRegistration.swift
//  BW22
//
//  Created by user on 23/04/2022.
//

import Foundation

struct UserRegistrationBody: Encodable {
    let first_name: String
    let last_name: String
    let email: String
    let phone_number: String
    let notification_token: String
    let image: String

    init(firstName: String,
         lastName: String,
         email: String,
         phoneNumber: String,
         notificationToken: String,
         image:String) {
        self.first_name = firstName
        self.last_name = lastName
        self.email = email
        self.phone_number = phoneNumber
        self.notification_token = notificationToken
        self.image = image
      
    }

    enum CodingKeys: String, CodingKey {
        case first_name, last_name, email, phone_number, notification_token, image
    }
}

struct UserRegistrationResponse: Codable{
    let referral_code: String
    let message: String
}
