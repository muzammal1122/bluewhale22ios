//
//  Email.swift
//  BW22
//
//  Created by PSE on 27/04/2022.
//

import Foundation

struct EmailBody : Encodable {
    let email : String
    let body : String
}

struct EmailResponse : Codable {
    let message : String
    let messageID : String
}
