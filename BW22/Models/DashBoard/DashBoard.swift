//
//  DashBoard.swift
//  BW22
//
//  Created by PSE on 26/04/2022.
//

import Foundation

struct DashBoardResponse : Codable {
    
    let market_cap_usdt_value : Int
    let change_in_24_hours_market_cap_percentage : Int
    let market_movement : [String : Double]
}
