//
//  ReferralCode.swift
//  BW22
//
//  Created by user on 25/04/2022.
//

import Foundation


struct ReferralCodeResponse : Codable{
    let referral_code: String
    let number_of_active_user: Int
    let number_of_followers: Int
    
}

struct SubscribeUserReferralDetailsBody:Encodable {
    let referral_code: String
    let phone_number: String
}

struct SubscribeUserReferralDetailsResponse:Codable {
    let message:String
}

struct UserReferralResponse : Codable {
        let referral_code : String
        let number_of_active_user : Int
        let  number_of_followers : Int
}
