//
//  Profile.swift
//  BW22
//
//  Created by PSE on 27/04/2022.
//

import Foundation


struct ProfileResponse : Codable {
    let message : String
}

struct ProfileBody : Encodable {
    
    let first_name : String
    let last_name : String
    let address : String
}
