//
//  AppDelegate.swift
//  BW22
//
//  Created by user on 19/04/2022.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {


    
    /// set orientations you want to be allowed in this property by default
    var orientationLock = UIInterfaceOrientationMask.portrait

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        print("didFinishLaunchingWithOptions")
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]){
            success, _ in
            
            guard success else{
                print("APN registry error")
                return
            }
            print("APN successfully registered")
        }
        
        application.registerForRemoteNotifications()
                
       // initVC()
       // let targetLang = UserDefaults.standard.object(forKey: "selectedLanguage") as? String
              //  Bundle.setLanguage((targetLang != nil) ? targetLang! : "en")
            
        
        return true
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        messaging.token{
            token, _ in
            
            guard let token = token else {
                print("Error in generating FCM token")
                return
            }
            print("FCM Token: \(token)")
        }
    }
    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        print("configurationForConnecting")
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
        print("didDiscardSceneSessions")

        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
   
    
    func initVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            
        
        let viewController:UIViewController?;
        
        let isUserFirstTimeInstalledApp = UDM.shared.getBoolValue(Constants.isUserFirstTimeInstalledApp)
        let userAccountStatus = UDM.shared.getStringValue(Constants.userAccountStatus)
        
        print ("LaunchAction:  isUserFirstTimeInstalledApp: \(isUserFirstTimeInstalledApp )")
        print ("LaunchAction:  userAccountStatus: " + (userAccountStatus))
        
        // When user visit app for the very first time
        if !(isUserFirstTimeInstalledApp) {
           viewController =  storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        }
        
        
        // When User phone authentication completed
        else if (userAccountStatus) != "nil" {
            
            // When user authenticated but needs to be give 4-digit code
            if (userAccountStatus) == Constants.loggedIn {
                viewController =  storyboard.instantiateViewController(withIdentifier: "LoginCodeViewController") as! LoginCodeViewController
            }
            
            else if (userAccountStatus) == Constants.shouldRegister {
                // User needs to be registered first
                viewController =  storyboard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
            }
            
            else if (userAccountStatus) == Constants.shouldSetLoginCode {
                viewController =  storyboard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            }
            else {
                // User needs to be provide referral code
                viewController =  storyboard.instantiateViewController(withIdentifier: "ReferralCodeViewController") as! ReferralCodeViewController
            }
            
            
           
        }
        else {
            // When User enter app second time but did't yet authenticated
            viewController =  storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        }
    
        viewController!.modalPresentationStyle = .custom
            
       // self.window?.rootViewController = viewController
        
       // windowScene.windows
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let mWindow = windowScene?.windows.first
        
        guard let nWindow = mWindow else {
            print("Window not found")
            return
        }
        
        nWindow.rootViewController = viewController
    }


}

extension String {
    var localized:String {
        return NSLocalizedString(self, comment: "")
    }
}
