//
//  PrizeViewController.swift
//  BW22
//
//  Created by PSE on 22/04/2022.
//

import UIKit

class PrizeViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
   
    
    
    @IBAction func btnGOTIT(_ sender: Any) {
        UDM.shared.saveBool(true, "isDialogShow")
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //adding an overlay to the view to give focus to the dialog box
        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
    }

    //MARK:- functions for the viewController
    static func showPopup(parentVC: UIViewController){
        print("alert")
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "PrizeViewController") as? PrizeViewController {
        popupViewController.modalPresentationStyle = .custom
        popupViewController.modalTransitionStyle = .crossDissolve
            parentVC.present(popupViewController, animated: true)
        
        }
      }
    }
