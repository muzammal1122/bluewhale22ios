//
//  CustomTableViewCell.swift
//  BW22
//
//  Created by PSE on 27/06/2022.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet var ivDetails: UIImageView!
    
    @IBOutlet weak var txtDetails: UILabel!
    
}
