//
//  NetworkClient.swift
//  BW22
//
//  Created by user on 23/04/2022.
//

import Foundation

struct Constants {
    static let userRegistrationURL = URL(string:"https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/register-user")
    static let UserRegisrationURLRequest = URLRequest(url: userRegistrationURL!)
    
    
    static let userloginURL = URL(string:"https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/login-user")
    static let UserLoginURL = URLRequest(url: userloginURL!)

    
    static let subscribeReferralCode = URL(string:"https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/update_user_referral_details")
    static let SubscribeReferralURL = URLRequest(url: subscribeReferralCode!)
    
    static let addMoreURL = URL(string:"https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/add-booking")
    static let AddMoreURL = URLRequest(url: addMoreURL!)
    
    
    static let RequestReferralCodeURL = "https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/get_referral_code/"
    
    static let UserStatsURL = "https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/user-stats/"
    
    static let RequestDashBoardData = "https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/market_movement"
    
    static let RequestProfileUpdate = "https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/update-user/"
    
    static let GetNews = "https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/get_news"
    
    static let SendEmail = URL(string: "https://11c5lm9gdf.execute-api.us-east-1.amazonaws.com/dev/send_email")
    static let SendEmailURL = URLRequest(url: SendEmail!)
    
    
    static let SharingReferralCodeURL = "https://www.bluewhale.host/ref.php?refcode="
    
    
    
    // Transition
    static let isUserFirstTimeInstalledApp = "isUserFirstTimeInstalledApp"
    static let isUserAlreadyRegistered = "isUserAlreadyRegistered"
    static let isUserAuthenticated = "isUserAuthenticated"
    static let userAccountStatus = "userAccountStatus"
    static let shouldRegister = "shouldRegister"
    static let shouldSetLoginCode = "shouldSetLoginCode"
    static let shouldGetReferralCode = "shouldGetReferralCode"
    static let loggedIn = "loggedIn"
    static let phoneNumber = "phoneNumber"
    static let referralCode = "referralCode"
    static let firstName = "firstName"
    static let lastName =  "lastName"
    static let email = "email"
    static let isFirstTimeCodeSetting = "isFirstTimeCodeSetting"
    static let appSecureCredentials = "appSecureCredentials"
    
    
    
    
    
    // Locals
    
    static let userTotalBalance = "userTotalBalance"
    static let userUSDT = "userUSDT"
    static let changeInPercentage = "changeInPercentage"
    static let usdt = "usdt"
    static let prebooking = "prebooking"
    static let marketCap = "marketCap"
    static let isUP = "isUP"
    static let preBookingChangeInLast24Hours = "preBookingChangeInLast24Hours"
    static let referralCodeLocal = "referralCodeLocal"
    static let followers = "followers"
    static let actives = "actives"
    static let BookedAmount = "booked_amount"
    
    
    
}
enum CustomError:Error {
    case invalidURL
    case invalidData
    case invalidBodyJson
    case apiError(message:String)
}
extension URLSession {
    
    func request <T: Codable> (
         url: URL?,
         model:WrappedResponse <T>.Type,
         callback: @escaping (Result<T, Error>) ->  Void){
             
             guard let url = url else {
                 callback(.failure(CustomError.invalidURL))
                 return
             }
             
             let task = dataTask(with: url){data,_, error in
                 guard let data = data else{
                     if let error = error {
                         callback(.failure(error))
                     } else{
                         callback(.failure(CustomError.invalidData))
                     }
                     return
                 }
                 
                 
                 do {
                     print("Called in API")
                     
                     let result  = try JSONDecoder().decode(model, from: data)
                     if(result.response.status == "SUCCESS"){
                         callback(.success(result.data!))
                     }else{
                         callback(.failure(ApiError(errorMessage: result.response.message!)))
                     }
                     
    
                 }catch{
                     print(error)
                     callback(.failure(error))
                 }
                 
             }
             task.resume()
             
            
    }
    
    
    
    func requestUpload <T: Codable, E:Encodable> (
         url: URLRequest?,
         type: String,
         body: E,
         model:WrappedResponse <T>.Type,
         callback: @escaping (Result<T, Error>) ->  Void){
             
             guard var url = url else {
                 callback(.failure(CustomError.invalidURL))
                 return
             }
             
             
             url.httpMethod = type
             url.addValue("application/json", forHTTPHeaderField: "Content-Type")
             url.addValue("application/json", forHTTPHeaderField: "Accept")
             
             do {
                      let jsonData = try JSONEncoder().encode(body)
                      url.httpBody = jsonData
                  } catch {
                      callback(.failure(CustomError.invalidBodyJson))
                      return
                  }
             
             let task = dataTask(with: url){data,_, error in
                 guard let data = data else{
                     if let error = error {
                         callback(.failure(error))
                     } else{
                         callback(.failure(CustomError.invalidData))
                     }
                     return
                 }
                 
                 
                 do {
                     
                     let result  = try JSONDecoder().decode(model, from: data)
                     if(result.response.status == "SUCCESS"){
                         callback(.success(result.data!))
                     }else{
                         callback(.failure(ApiError(errorMessage: result.response.message!)))
                     }
                     
    
                 }catch{
                     print(error)
                     callback(.failure(error))
                 }
                 
             }
             task.resume()
             
            
    }
}

struct WrappedResponse <T:Codable> : Codable {
    let response:  Response
    let data: T?
}

struct Response:Codable {
    let status : String
    let message : String?
}
struct ApiError:Error{
    var errorMessage:String
    init (errorMessage:String){
        self.errorMessage = errorMessage
    }
}
