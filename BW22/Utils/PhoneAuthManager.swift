//
//  PhoneAuthManager.swift
//  BW22
//
//  Created by user on 21/04/2022.
//

import Foundation
import Firebase

class PhoneAuthManager {
    static let shared = PhoneAuthManager()
    private var verificationId: String?
    private let auth = Auth.auth()
    
    public func startAuth (_ phoneNumber: String, completion: @escaping (Result<String, Error>) -> Void){
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil ){
            [weak self] verificationID , error in
            guard let verificationId = verificationID else {
                guard let error = error else {
                    completion(.failure(AuthError.init(errorMessage: "Phone Authentication error")))
                    return
                }
                completion(.failure(AuthError.init(errorMessage: error.localizedDescription)))
                return
            }
            self?.verificationId = verificationID
            completion(.success(verificationId))
            
        }
    }
    
    public func verifyCode(_ smsCode: String, completion: @escaping (Result<Bool,Error>) -> Void){
    
        guard let verificationID  = self.verificationId else {
            completion(.failure(AuthError.init(errorMessage: "Verification ID not found")))
            return
        }
        
        let credentials = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode:smsCode)
        auth.signIn(with: credentials){
             result, error in
            
            guard result != nil ,error == nil else {
                guard let error  = error else  {
                    completion(.failure(AuthError.init(errorMessage: "Phone number signing in error")))
                    return
                }
                completion(.failure(AuthError.init(errorMessage: error.localizedDescription)))
                return
            }
            
            completion(.success(true))
            
            
        }
    
    }
    
    
    
    
}


struct AuthError:Error{
    var errorMessage:String
    init (errorMessage:String){
        self.errorMessage = errorMessage
    }
}
