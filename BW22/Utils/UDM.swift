//
//  UDM.swift
//  BW22
//
//  Created by user on 23/04/2022.
//

import Foundation

class UDM {
    static let shared = UDM()
    
    let defaults = UserDefaults.standard
    
    func saveString(_ value: String,_ key:String){

        defaults.setValue(value, forKey: key)
    }
    
    func getStringValue(_ key:String) -> String{

        guard let value = defaults.value(forKey: key) as? String else {
            return "nil"
        }
        return value
    }
    
    func saveBool(_ value: Bool,_ key:String){

        defaults.setValue(value, forKey: key)
    }
    
    func getBoolValue(_ key:String) -> Bool{
        guard let value = defaults.value(forKey: key) as? Bool else {
            return false
        }
        return value
    }
    
    func saveInt(_ value: Int,_ key:String){

        defaults.setValue(value, forKey: key)
    }
    
    func getIntValue(_ key:String) -> Int{

        guard let value = defaults.value(forKey: key) as? Int else {
            return 0
        }
        return value
    }
    
}
